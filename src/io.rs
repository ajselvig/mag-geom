use serde::Serialize;
use std::io;

pub trait CsvExport {


    fn to_csv_writer<W: io::Write>(&self, writer: W) -> csv::Writer<W>;

    fn to_csv_string(&self) -> String {
        let mut s = vec![];
        let wtr = self.to_csv_writer(s);
        String::from_utf8(wtr.into_inner().unwrap()).unwrap()
    }
}

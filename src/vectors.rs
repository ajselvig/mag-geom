pub type Vector3 = cgmath::Vector3<f64>;
pub use cgmath::prelude::*;
pub use cgmath::num_traits::identities::Zero;
pub use cgmath::vec3;
pub use cgmath::Rotation;
pub use cgmath::Rotation3;
pub use cgmath::Rad;
pub use cgmath::Basis3;

pub type Scalar = f64;
pub type ScalarData = Vec<Scalar>;
pub type VectorData3 = Vec<Vector3>;


pub trait VectorSizing<T> {

    // the scalar magnitude of the vector
    fn mag(&self) -> Scalar;

    // the square of the scalar magnitude of the vector
    fn mag_squared(&self) -> Scalar;

    // creates a unit vector in the same direction
    fn unit(&self) -> T;

}


impl VectorSizing<Vector3> for Vector3 {
    fn mag(&self) -> f64 {
        self.mag_squared().sqrt()
    }

    fn mag_squared(&self) -> f64 {
        self.x*self.x + self.y*self.y + self.z*self.z
    }

    fn unit(&self) -> Vector3 {
        let len = self.mag();
        vec3(self.x / len, self.y / len, self.z/len)
    }
}


/// Tests

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sizing() {
        let v = vec3(1.0, 2.0, 3.0);
        assert_eq!(v.mag(), 14.0_f64.sqrt());

        assert_eq!(v.unit().mag(), 1.0);
    }
}
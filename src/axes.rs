use super::vectors::*;

pub type AxisRange = (f64, f64);
pub type AxisCoords = Vec<f64>;

pub struct Axes3 {
    pub x: AxisCoords,
    pub y: AxisCoords,
    pub z: AxisCoords
}

pub struct AxesCoord3 {
    pub vector: Vector3,
    pub index: usize
}

fn range_to_axis(range: AxisRange, step: f64) -> AxisCoords {
    let n = ((range.1 - range.0)/step).ceil() as usize;
    let mut coords = AxisCoords::with_capacity(n);
    for i in 0..(n+1) {
        coords.push((i as f64)*step + range.0)
    }
    coords
}


impl Axes3 {

    pub fn build(x_range: AxisRange,
                 y_range: AxisRange,
                 z_range: AxisRange,
                 step: f64) -> Axes3 {
        let x = range_to_axis(x_range, step);
        let y = range_to_axis(y_range, step);
        let z = range_to_axis(z_range, step);
        Axes3 {x, y, z}
    }

    pub fn data_size(&self) -> usize {
        self.x.len() * self.y.len() * self.z.len()
    }

    pub fn data_index(&self, i: usize, j: usize, k: usize) -> usize {
        i + j*(self.x.len()) + k*(self.x.len()*self.y.len())
    }

    pub fn all_coords(&self) -> Vec<AxesCoord3> {
        let mut coords = Vec::with_capacity(self.data_size());
        let mut i = 0;
        for z in &self.z {
            for y in &self.y {
                for x in &self.x {
                    coords.push(AxesCoord3 {index: i, vector: Vector3::new(*x, *y, *z)});
                    i += 1;
                }
            }
        }
        coords
    }

    pub fn closest_indices(coords: &AxisCoords, value: Scalar) -> (usize,usize) {
        for i in 1..coords.len() {
            if value < coords[i] {
                return (i-1,i)
            }
        }
        (coords.len()-2, coords.len()-1)
    }

}


/// Tests

#[cfg(test)]
mod tests {
    use super::*;

    fn setup() -> Axes3 {
        Axes3::build((-1.0, 1.0), (-1.0, 1.0), (-1.0, 1.0), 0.1)
    }

    #[test]
    fn size() {
        let axes = setup();
        assert_eq!(axes.x.len(), 21);
        assert_eq!(axes.x[0], -1.0);
        assert_eq!(axes.x[20], 1.0);
    }

    #[test]
    fn index_lookup() {
        let axes = setup();
        let indices = Axes3::closest_indices(&axes.x, -0.75);
        assert_eq!(indices.0, 2);
        assert_eq!(indices.1, 3);
    }
}
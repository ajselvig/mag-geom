use std::f64;

pub const PI: f64 = std::f64::consts::PI;

// permeability of free space
pub const MU_0: f64 = 4.0 * PI * 10e-7;

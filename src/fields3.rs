use std::ops::Mul;
use std::ops::Add;

use serde::Serialize;
use std::io;

use super::axes::*;
use super::vectors::*;
use super::io::*;

/// Field Base

pub trait FieldBase3<T> where T: Add + Copy + Mul<Scalar>,
                              <T as Mul<Scalar>>::Output: Add<Output=T> {

    fn axes(&self) -> &Axes3;

    fn data(&self) -> &Vec<T>;

    fn default_value() -> T;

    fn build_data(axes: &Axes3) -> Vec<T> {
        let n = axes.data_size();
        let mut data = Vec::with_capacity(n);
        for _ in 0..n {
            data.push(Self::default_value());
        }
        data
    }

    fn set(&mut self, index: usize, value: T);

    fn at(&self, i: usize, j: usize, k: usize) -> T {
        let index = self.axes().data_index(i, j, k);
        self.data()[index]
    }

    fn populate<F: Fn(&Vector3) -> T>(&mut self, f: F) {
        for coord in self.axes().all_coords() {
            let value = f(&coord.vector);
            self.set(coord.index, value);
        }
    }

    // https://en.wikipedia.org/wiki/Trilinear_interpolation
    fn interpolate(&self, v: &Vector3) -> T {
        let axes = self.axes();

        let (i0,i1) = Axes3::closest_indices(&axes.x, v.x);
        let (j0,j1) = Axes3::closest_indices(&axes.y, v.y);
        let (k0,k1) = Axes3::closest_indices(&axes.z, v.z);

        let x0 = axes.x[i0];
        let x1 = axes.x[i1];
        let y0 = axes.y[j0];
        let y1 = axes.y[j1];
        let z0 = axes.z[k0];
        let z1 = axes.z[k1];

        let xd = (v.x - x0) / (x1 - x0);
        let yd = (v.y - y0) / (y1 - y0);
        let zd = (v.z - z0) / (z1 - z0);

        let c000 = self.at(i0, j0, k0);
        let c100 = self.at(i1, j0, k0);
        let c110 = self.at(i1, j1, k0);
        let c111 = self.at(i1, j1, k1);
        let c010 = self.at(i0, j1, k0);
        let c011 = self.at(i0, j1, k1);
        let c001 = self.at(i0, j0, k1);
        let c101 = self.at(i1, j0, k1);

        let xd_inv = 1.0 - xd;
        let c00 = c000*xd_inv + c100*xd;
        let c01 = c001*xd_inv + c101*xd;
        let c10 = c010*xd_inv + c110*xd;
        let c11 = c011*xd_inv + c111*xd;

        let yd_inv = 1.0 - yd;
        let c0 = c00*yd_inv + c10*yd;
        let c1 = c01*yd_inv + c11*yd;

        c0*(1.0 - zd) + c1*zd
    }
}


/// Scalar Field

pub struct ScalarField3 {
    pub axes: Axes3,
    pub data: ScalarData
}

#[derive(Debug, Serialize)]
struct ScalarRecord {
    i: usize,
    x: f64,
    y: f64,
    z: f64,
    s: f64
}

impl ScalarField3 {

    pub fn build(axes: Axes3) -> ScalarField3 {
        let data = Self::build_data(&axes);
        ScalarField3 {axes, data}
    }

}

impl FieldBase3<Scalar> for ScalarField3 {
    fn axes(&self) -> &Axes3 {
        &self.axes
    }

    fn data(&self) -> &Vec<Scalar> {
        &self.data
    }

    fn default_value() -> Scalar {
        0.0
    }

    fn set(&mut self, index: usize, value: Scalar) {
        self.data[index] = value;
    }
}

impl CsvExport for ScalarField3 {
    fn to_csv_writer<W: io::Write>(&self, writer: W) -> csv::Writer<W> {
        let mut wtr = csv::Writer::from_writer(writer);
        for coord in self.axes.all_coords() {
            let s = self.data()[coord.index];
            wtr.serialize(ScalarRecord {
                i: coord.index,
                x: coord.vector.x,
                y: coord.vector.y,
                z: coord.vector.z,
                s
            }).unwrap();
        }
        wtr.flush().unwrap();
        wtr
    }
}


/// Vector Field

pub struct VectorField3 {
    pub axes: Axes3,
    pub data: VectorData3
}

#[derive(Debug, Serialize)]
struct VectorRecord {
    i: usize,
    x: f64,
    y: f64,
    z: f64,
    vx: f64,
    vy: f64,
    vz: f64
}

impl VectorField3 {

    pub fn build(axes: Axes3) -> VectorField3 {
        let data = Self::build_data(&axes);
        VectorField3 {axes, data}
    }

    pub fn populate<F: Fn(&Vector3) -> Vector3>(&mut self, f: F) {
        for coord in self.axes.all_coords() {
            let value = f(&coord.vector);
            self.data[coord.index] = value;
        }
    }

}

impl FieldBase3<Vector3> for VectorField3 {
    fn axes(&self) -> &Axes3 {
        &self.axes
    }

    fn data(&self) -> &Vec<Vector3> {
        &self.data
    }

    fn default_value() -> Vector3 {
        Vector3::zero()
    }

    fn set(&mut self, index: usize, value: Vector3) {
        self.data[index] = value;
    }
}


impl CsvExport for VectorField3 {
    fn to_csv_writer<W: io::Write>(&self, writer: W) -> csv::Writer<W> {
        let mut wtr = csv::Writer::from_writer(writer);
        for coord in self.axes.all_coords() {
            let v = self.data()[coord.index];
            wtr.serialize(VectorRecord {
                i: coord.index,
                x: coord.vector.x,
                y: coord.vector.y,
                z: coord.vector.z,
                vx: v.x,
                vy: v.y,
                vz: v.z
            }).unwrap();
        }
        wtr.flush().unwrap();
        wtr
    }
}


/// Tests

#[cfg(test)]
mod tests {
    use super::*;

    fn setup() -> ScalarField3 {
        let axes = Axes3::build((-1.0, 1.0), (-1.0, 1.0), (-1.0, 1.0), 0.1);
        ScalarField3::build(axes)
    }

    #[test]
    fn size() {
        let field = setup();
        assert_eq!(field.data.len(), 9261)
    }

    #[test]
    fn interpolation() {
        let mut field = setup();

        let fun = |v: &Vector3| -> Scalar {
            v.x + v.y*2.0 - v.z
        };

        field.populate(fun);

        let v = vec3(0.55, 0.25, 0.35);
        let truth = fun(&v);
        let computed = field.interpolate(&v);
        assert_eq!(computed, truth);
    }

    #[test]
    fn csv() {
        let field = setup();
        let s = field.to_csv_string();
        let mut lines = s.lines();
        let header = lines.next();
        assert_eq!(Some("i,x,y,z,s"), header);
        let mut n = 0;
        while lines.next() != None {
            n += 1;
        }
        assert_eq!(n, field.data().len());
    }
}
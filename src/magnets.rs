use serde::Serialize;
use std::io;

use super::constants::*;
use super::vectors::*;
use super::fields3::*;
use super::io::*;

use assert_approx_eq::assert_approx_eq;

/// Current Segment

struct CurrentSegment {
    position: Vector3,
    direction: Vector3,
    length: f64
}


// mu_0/4*pi, so we don't have to do the math each time
const FIELD_MULTIPLIER: f64 = MU_0 / (4.0*PI);

impl CurrentSegment {

    pub fn field_at(&self, p: Vector3, current: f64) -> Vector3 {
        let r = p - self.position;
        self.direction.cross(r.unit()) * FIELD_MULTIPLIER * current * self.length / r.mag_squared()
    }

}


/// Current Loop

struct CurrentLoop {
    segments: Vec<CurrentSegment>,
    current: f64
}

impl CurrentLoop {

    pub fn build_ring(center: Vector3, direction: Vector3, radius: f64, num_segments: usize) -> CurrentLoop {
        let mut current_loop = CurrentLoop {
            segments: Vec::with_capacity(num_segments),
            current: 1.0
        };
        let dir_unit = direction.unit();

        // get an arbitrary unit vector normal to the direction
        let mut r = direction.cross(vec3(direction.y, direction.z, direction.x)).unit();
        let d_angle = PI*2.0/(num_segments as f64);
        let rotation : Basis3<Scalar> = Rotation3::from_axis_angle(direction, Rad(d_angle));
        let segment_length = d_angle * radius;
        for i in 0..num_segments {
            let p = center + r*radius;
            current_loop.segments.push(CurrentSegment {
                position: p,
                direction: direction.cross(r),
                length: segment_length
            });
            r = rotation.rotate_vector(r);
        }
        current_loop
    }


    // computes the field at the given point due to this loop
    pub fn field_at(&self, p: Vector3) -> Vector3 {
        let mut b = Vector3::zero();
        for segment in &self.segments {
            b = b + segment.field_at(p, self.current);
        }
        b
    }

}


#[derive(Debug, Serialize)]
struct CurrentSegmentRecord {
    length: f64,
    px: f64,
    py: f64,
    pz: f64,
    dx: f64,
    dy: f64,
    dz: f64
}

impl CsvExport for CurrentLoop {
    fn to_csv_writer<W: io::Write>(&self, writer: W) -> csv::Writer<W> {
        let mut wtr = csv::Writer::from_writer(writer);
        for segment in &self.segments {
            wtr.serialize(CurrentSegmentRecord {
                length: segment.length,
                px: segment.position.x,
                py: segment.position.y,
                pz: segment.position.z,
                dx: segment.direction.x,
                dy: segment.direction.y,
                dz: segment.direction.z
            }).unwrap();
        }
        wtr.flush().unwrap();
        wtr
    }
}


/// Tests

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ring() {
        let center = vec3(0.0, 1.0, 0.0);
        let direction = vec3(0.0, 0.0, 1.0);
        let num_segments = 128;
        let radius = 2.0;
        let mut ring_loop = CurrentLoop::build_ring(center, direction, radius, num_segments);
        ring_loop.current = 1000.0;

        assert_eq!(ring_loop.segments.len(), num_segments);

        // field at the center of a ring is mu_0*I/2*R
        let center_field = ring_loop.field_at(center);
        let reference_center_field_mag = MU_0 * ring_loop.current / (radius*2.0);
        assert_approx_eq!(center_field.mag(), reference_center_field_mag, 1e-8f64);
        assert_eq!(center_field.unit(), direction);

        // field along center line is mu_0*I*R^2/2*(R^2+z^2)^3/2
        let dz = 0.5;
        let r_squared = radius*radius;
        let reference_offset_field = MU_0 * ring_loop.current * r_squared / (2.0*(r_squared + dz*dz).powf(1.5));
        let offset_field = ring_loop.field_at(center + direction*dz);
        assert_approx_eq!(offset_field.mag(), reference_offset_field, 1e-8f64);
        assert_eq!(offset_field.unit().z, 1.0);
    }

    #[test]
    fn csv() {
        let num_segments = 128;
        let ring_loop = CurrentLoop::build_ring(vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 1.0), 1.0, num_segments);

        let s = ring_loop.to_csv_string();
        let mut lines = s.lines();
        let header = lines.next();
        assert_eq!(Some("length,px,py,pz,dx,dy,dz"), header);
        let mut n = 0;
        while lines.next() != None {
            n += 1;
        }
        assert_eq!(n, num_segments);
    }

}